function createCard(name, description, pictureUrl, startDate, endDate, location) {
    const start=new Date(startDate);
    const end = new Date(endDate);
    const startStr=start.toLocaleDateString();
    const endStr=end.toLocaleDateString();
    return `
      <div class="shadow-lg p-3 mb-5 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-text">${description}</p>
          <p class="card-text">${location}</p>
          <p class="card-text"><small class="text-muted">${startStr} - ${endStr}</small></p>
        </div>
      </div>
    `;
  }

  // window.addEventListener('DOMContentLoaded', async () => {

  //   const url = 'http://localhost:8000/api/conferences/';

  //   try {
  //     const response = await fetch(url);
  //     console.log(response)
  //     if (!response.ok) {
  //       throw new Error(e= 'Response not ok')
  //       // Figure out what to do when the response is bad
  //     } else {
  //       const data = await response.json();
  //       const conference=data.conferences[2];
  //       const nameTag=document.querySelector('.card-title');
  //       nameTag.innerHTML=conference.name;

  //       const detailUrl=`http://localhost:8000${conference.href}`;
  //       const detailResponse= await fetch(detailUrl);
  //       if (detailResponse.ok){
  //         const details = await detailResponse.json();
  //         const description=details.conference.description
  //         const descriptionTag = document.querySelector('.card-text');
  //         descriptionTag.innerHTML = description;


  //         const imgTag= document.querySelector('.card-img-top')
  //         imgTag.src = details.conference.location.picture_url;
  //         imgTag.innerHTML=details.conference.location.picture_url;
  //         console.log(details);
  //       }
  //       console.log(conference)
  //     }
  //   } catch (e) {
  //     console.log(e);
  //     // Figure out what to do if an error is raised
  //   }

  // });





  // window.addEventListener('DOMContentLoaded', async() => {
  //     const url ='http://localhost:8000/api/conferences/';
  //     const response = await fetch(url);
  //     console.log(response);

  //     const data = await response.json();
  //     console.log(data);
  // });

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw new Error(e= 'Response not ok')
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = details.conference.starts;
            const endDate= details.conference.ends;
            const location = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, startDate, endDate, location);
            const row = document.querySelector('.row');
            row.innerHTML += html;
          }
        }

      }
    } catch (e) {
      console.log(e);
      // Figure out what to do if an error is raised
    }

  });
