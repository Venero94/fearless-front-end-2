import React from 'react';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div>
        <Routes>
          <Route path="main">
          <Route index element={<MainPage />} />
          </Route>
          <Route path="locations">
            <Route path="new" element={<LocationForm/>}/>
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm/>}/>
          </Route>
          <Route path="attendees">
            <Route path="new" element={<AttendConferenceForm/>}/>
            <Route path="" element={<AttendeesList attendees={props.attendees}/>}/>
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }
//   return (
//     <BrowserRouter>
//       <Nav />
//       <div className="container">
//           <Routes>
//             <Route path="conferences/new" element={<ConferenceForm />} />
//             <Route path="attendees/new" element={<AttendConferenceForm />} />
//             <Route path="locations/new" element={<LocationForm />} />
//             <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
//           </Routes>
//       </div>
//     </BrowserRouter>
//   );
// }


// export default App;



// import Nav from './Nav';
// import React from 'react';


// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }
//   return (
//     <React.Fragment>
//       <Nav />
//       <div className="container">
//         <table className="table table-striped">
//           <thead>
//             <tr>
//               <th>Name</th>
//               <th>Conference</th>
//             </tr>
//           </thead>
//           <tbody>
//             {/* for (let attendee of props.attendees) {
//               <tr>
//                 <td>{ attendee.name }</td>
//                 <td>{ attendee.conference }</td>
//               </tr>
//             } */}
//             {props.attendees.map(attendee => {
//               return (
//                 <tr key={attendee.href}>
//                   <td>{ attendee.name }</td>
//                   <td>{ attendee.conference }</td>
//                 </tr>
//               );
//             })}
//           </tbody>
//         </table>
//       </div>
//     </React.Fragment>
//   );
// }

// export default App;
